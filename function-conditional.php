<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php
    echo "<h3> Soal No 1 Greetings </h3>";

    function greetings($nama)
    {
        $pesan = "Halo " . ucfirst($nama) . ", Selamat Datang di Garuda Cyber Institute!";
        echo $pesan;
    }

    greetings("JHON");
    echo "<br>";
    greetings("FERRY");
    echo "<br>";
    greetings("WILSON");

    echo "<h3>Soal No 2 Reverse String</h3>";
    function reverseString($input)
    {
        $panjang = strlen($input);
        $hasil = '';

        for ($i = $panjang - 1; $i >= 0; $i--) {
            $hasil .= $input[$i];
        }

        echo $hasil;
    }

    reverseString("nama peserta");
    echo "<br>";
    reverseString("Garuda Cyber Institute");
    echo "<br>";
    reverseString("We Are GC-Ins Developer");

    echo "<h3>Soal No 3 Palindrome </h3>";
    function reverseStringPalindrome($input)
    {
        $panjang = strlen($input);
        $hasil = '';

        for ($i = $panjang - 1; $i >= 0; $i--) {
            $hasil .= $input[$i];
        }

        return $hasil;
    }

    function palindrome($str)
    {
        $str = strtolower($str);
        $reverse = reverseStringPalindrome($str);

        return $str === $reverse;
    }

    
    echo var_export(palindrome("civic"), true); 
    echo "<br>";
    echo var_export(palindrome("nababan"), true); 
    echo "<br>";
    echo var_export(palindrome("jambaban"), true); 
    echo "<br>";
    echo var_export(palindrome("racecar"), true); 

    echo "<h3>Soal No 4 Tentukan Nilai </h3>";
    function tentukan_nilai($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            return "Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            return "Cukup";
        } else {
            return "Kurang";
        }
    }

   
    echo tentukan_nilai(98);
    echo "<br>";
    echo tentukan_nilai(76);
    echo "<br>";
    echo tentukan_nilai(67); 
    echo "<br>";
    echo tentukan_nilai(43); 
    ?>
</body>

</html>
